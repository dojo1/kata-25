def generate_outcomes(input, output = [""])
  return output if input.empty?

  current_char = input[0]
  input[0] = ''

  if current_char != '?'
    generate_outcomes(input, output.map{ |element| "#{element}#{current_char}" })
  else
    output_1 = generate_outcomes(input.dup, output.map{ |element| "#{element}0" })
    output_2 = generate_outcomes(input.dup, output.map{ |element| "#{element}1" })
    output = output_1 + output_2
  end
end
